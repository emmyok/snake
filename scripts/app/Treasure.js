define(['config'], function (config) {

    function Treasure() {
        this.drawRandomTreasure();
    }

    Treasure.prototype.drawRandomTreasure = function () {
        this.container = new PIXI.Container();
        var randomColumn = this.randomSquare(config.numberOfColumns);
        var randomRow = this.randomSquare(config.numberOfRows);
        this.create(randomColumn, randomRow);
        config.treasurePosition.column = randomColumn;
        config.treasurePosition.row = randomRow;
        config.stage.addChild(this.container);
    };

    Treasure.prototype.create = function (column, row) {
        var graphics = new PIXI.Graphics();
        graphics.beginFill(config.treasureColor);
        graphics.lineStyle(2, config.squaresBackColor);
        graphics.x = column * config.squareDimension;
        graphics.y = row * config.squareDimension;
        graphics.drawRect(0, 0, 20, 20);
        this.container.addChild(graphics);
    };

    Treasure.prototype.randomSquare = function (randomNumber) {
        return Math.floor(Math.random() * randomNumber);
    };

    Treasure.prototype.clean = function () {
        this.container.removeChildren();
        config.stage.removeChild(this.container);
    };

    return new Treasure();
});