define(['pixi', 'Background', 'Treasure', 'Snake'], function (pixi, Background, Treasure, Snake) {
    function init() {
        new Background();
        var snake = new Snake();
        pixi.setSnake(snake);

    };

    init();
});