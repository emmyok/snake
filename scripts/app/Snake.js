define(['config', 'Treasure'], function (config, treasure) {
    function Snake() {
        this.container = new PIXI.Container();
        this.positionSnake = [];
        this.direction = 'right';
        this.drawSnake();
        this.PressKeyBoard();
    };

    Snake.prototype.drawSnake = function () {
        var snakeBody = this.create(1, 1);
        this.positionSnake.push({column: 1, row: 1});
        this.container.addChild(snakeBody);
        config.stage.addChild(this.container);
    };

    Snake.prototype.create = function (column, row) {
        var snakeBody = new PIXI.Graphics();
        snakeBody.beginFill(config.snakeColor);
        snakeBody.lineStyle(2, config.squaresBackColor);
        snakeBody.x = column * config.squareDimension;
        snakeBody.y = row * config.squareDimension;
        snakeBody.drawRect(0, 0, 20, 20);
        return snakeBody;
    };

    Snake.prototype.checkIfNewRound = function () {
        return (this.positionSnake[0].column > config.numberOfColumns - 1 || this.positionSnake[0].row > config.numberOfRows - 1 || this.positionSnake[0].column < 0 || this.positionSnake[0].row < 0);
    };


    Snake.prototype.checkIfHeadHitBody = function () {

        if (this.positionSnake.length > 1) {

            if (this.direction === 'left') {
                for (var i = 1; i < this.positionSnake.length; i++) {
                    if (this.positionSnake[0].column - 1 === this.positionSnake[i].column && this.positionSnake[0].row === this.positionSnake[i].row) {
                        return true;
                    }
                }
            }
            if (this.direction === 'right') {
                for (var i = 1; i < this.positionSnake.length; i++) {
                    if (this.positionSnake[0].column + 1 === this.positionSnake[i].column && this.positionSnake[0].row === this.positionSnake[i].row) {
                        return true;
                    }
                }
            }

            if (this.direction === 'up') {
                for (var i = 1; i < this.positionSnake.length; i++) {
                    if (this.positionSnake[0].row - 1 === this.positionSnake[i].row && this.positionSnake[0].column === this.positionSnake[i].column) {
                        return true;
                    }
                }
            }

            if (this.direction === 'down') {
                for (var i = 1; i < this.positionSnake.length; i++) {
                    if (this.positionSnake[0].row + 1 === this.positionSnake[i].row && this.positionSnake[0].column === this.positionSnake[i].column) {
                        return true;
                    }
                }
            }
        }
    };

    Snake.prototype.startGame = function () {
        treasure.clean();
        treasure.drawRandomTreasure();
        this.positionSnake = [];
        this.direction = 'right';
        this.drawSnake();
    };

    Snake.prototype.createSnake = function () {
        for (var i = 0; i < this.positionSnake.length; i++) {
            var snakeBody = this.create(this.positionSnake[i].column, this.positionSnake[i].row);
            this.container.addChild(snakeBody);
        }
    };

    Snake.prototype.changePosition = function () {
        if (this.positionSnake.length > 1) {
            for (var i = this.positionSnake.length - 1; i > 0; i--) {
                this.positionSnake[i] = JSON.parse(JSON.stringify(this.positionSnake[i - 1]));
            }
        }
        ;

        if (this.direction === 'up') {
            this.positionSnake[0].row--;
        }
        else if (this.direction === 'down') {
            this.positionSnake[0].row++;
        }
        else if (this.direction === 'left') {
            this.positionSnake[0].column--;
        }
        else if (this.direction === 'right') {
            this.positionSnake[0].column++;
        }
    };


    Snake.prototype.PressKeyBoard = function () {
        document.onkeydown = function (event) {
            event = event || window.event;

            if (event.keyCode === 38 && this.direction !== 'down') {
                this.direction = 'up';
            }
            else if (event.keyCode === 40 && this.direction !== 'up') {
                this.direction = 'down';

            }
            else if (event.keyCode === 37 && this.direction !== 'right') {
                this.direction = 'left';
            }
            else if (event.keyCode === 39 && this.direction !== 'left') {
                this.direction = 'right';
            }
        }.bind(this);
    };

    Snake.prototype.checkNextPositionIsFruit = function () {
        if (this.direction === 'left') {
            if (this.positionSnake[0].column - 1 === config.treasurePosition.column && this.positionSnake[0].row === config.treasurePosition.row) {
                return true;
            }
        }
        if (this.direction === 'right') {
            if (this.positionSnake[0].column + 1 === config.treasurePosition.column && this.positionSnake[0].row === config.treasurePosition.row) {
                return true;
            }
        }
        if (this.direction === 'up') {
            if (this.positionSnake[0].row - 1 === config.treasurePosition.row && this.positionSnake[0].column === config.treasurePosition.column) {
                return true;
            }
        }
        if (this.direction === 'down') {
            if (this.positionSnake[0].row + 1 === config.treasurePosition.row && this.positionSnake[0].column === config.treasurePosition.column) {
                return true;
            }
        }
        return false;
    }


    Snake.prototype.findFruitAndChangeDirection = function () {
        if (this.positionSnake[0].column < config.treasurePosition.column) {
            this.direction = 'right';
        } else if (this.positionSnake[0].row < config.treasurePosition.row) {
            this.direction = 'down';
        } else if (this.positionSnake[0].column > config.treasurePosition.column) {
            this.direction = 'left';
        } else if (this.positionSnake[0].row > config.treasurePosition.row) {
            this.direction = 'up';
        } else {
            this.direction = 'right';

        }
    };

    Snake.prototype.snakeMove = function () {
        this.container.removeChildren();

        if (config.autoplay) {
            this.findFruitAndChangeDirection();
        }
        ;

        if (this.checkIfNewRound() || this.checkIfHeadHitBody()) {
            this.startGame();
        } else {
            if (this.checkNextPositionIsFruit()) {
                this.positionSnake.unshift({
                    column: config.treasurePosition.column,
                    row: config.treasurePosition.row
                })
                treasure.container.removeChildren();
                treasure.drawRandomTreasure();
            } else {
                this.changePosition();
            }
            this.createSnake();
        }
    };

    return Snake;
});