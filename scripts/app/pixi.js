define(function () {
    function Pixi() {
        this.renderer;
        this.stage;
        this.create();
    };

    Pixi.prototype.gameLoop = function () {
        setTimeout(this.gameLoop.bind(this), 200);

        if (this.snake) {
            this.snake.snakeMove();
        }
        this.renderer.render(this.stage);
    };

    Pixi.prototype.create = function () {
        this.renderer = PIXI.autoDetectRenderer(801, 601);
        document.body.appendChild(this.renderer.view);
        this.stage = new PIXI.Container();
        this.gameLoop();
    };

    Pixi.prototype.getStage = function () {
        return this.stage;
    };

    Pixi.prototype.getRenderer = function () {
        return this.renderer;
    };

    Pixi.prototype.setSnake = function (snake) {
        return this.snake = snake;
    };

    return new Pixi();
});