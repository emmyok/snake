define(['config'], function (config) {
    function Background() {
        this.drawBackgroundSquares();
    };

    Background.prototype.drawBackgroundSquares = function () {
        this.backgroundContainer = new PIXI.Container();
        for (var column = 0; column < config.numberOfColumns; column++) {
            for (var row = 0; row < config.numberOfRows; row++) {
                this.create(column, row);
            }
            ;
        }
        ;
        config.stage.addChild(this.backgroundContainer);
    };

    Background.prototype.create = function (column, row) {
        var graphics = new PIXI.Graphics();
        graphics.lineStyle(2, config.squaresBackColor);
        graphics.x = column * config.squareDimension;
        graphics.y = row * config.squareDimension;
        graphics.drawRect(0, 0, 20, 20);
        this.backgroundContainer.addChild(graphics);
    };

    return Background;
});