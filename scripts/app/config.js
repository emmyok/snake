define(['pixi'], function (pixi) {

    function Config() {

        this.squaresBackColor = 0x050505;
        this.treasureColor = 0x0fffff;
        this.snakeColor = 0x0ff00f;
        this.squareDimension = 20;
        this.renderer = pixi.getRenderer();
        this.stage = pixi.getStage();
        this.numberOfColumns = this.renderer.width / this.squareDimension;
        this.numberOfRows = this.renderer.height / this.squareDimension;
        this.treasurePosition = {column: 0, row: 0};
        this.autoplay = false;
    };

    return new Config();
});



