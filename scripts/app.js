requirejs.config({
    baseUrl: 'scripts/app',
    paths: {
        app: "../app",
        pixijs: '../lib/pixi'
    }
});

requirejs(["pixijs"], function() {
    requirejs(["app/main"]);
});
